﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Moq;
using SportsStore.Models;
using System.Linq;
using SportsStore.Controllers;
using SportsStore.Models.ViewModels;

namespace SportStore.Tests
{
    [TestFixture]
    public class ProductControllerTests
    {
        [TestCase]
        public void Can_Padinate()
        {
            //Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[]{
                new Product {ProductID=1, Name="P1" },
                new Product {ProductID=2, Name="P2" },
                new Product {ProductID=3, Name="P3" },
                new Product {ProductID=4, Name="P4" },
                new Product {ProductID=5, Name="P5" }
            }).AsQueryable<Product>());

            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;

            //Act
            ProductListViewModel _result = controller.List(null, 2).ViewData.Model as ProductListViewModel;

            //Assert
            Product[] prodArray = _result.Products.ToArray();
            Assert.True(prodArray.Length == 2);
            Assert.AreEqual("P4", prodArray[0].Name);
            Assert.AreEqual("P5", prodArray[1].Name);
        }

        [TestCase]
        public void Can_Send_Pagination_View_Model()
        {
            //Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[] {
                new Product{ProductID=1, Name="P1" },
                new Product{ProductID=1, Name="P2" },
                new Product{ProductID=1, Name="P3" },
                new Product{ProductID=1, Name="P4" },
                new Product{ProductID=1, Name="P5" }
            }).AsQueryable<Product>());


            ProductController controller = new ProductController(mock.Object) { PageSize = 3 };

            //Act
            ProductListViewModel result = controller.List(null, 2).ViewData.Model as ProductListViewModel;

            //Assert
            PagingInfo _pageInfo = result.PagingInfo;
            Assert.AreEqual(2, _pageInfo.CurrentPage);
            Assert.AreEqual(3, _pageInfo.ItemPerPage);
            Assert.AreEqual(5, _pageInfo.TotalItems);
            Assert.AreEqual(2, _pageInfo.TotalPages);
        }

        [TestCase]
        public void Can_Filter_Product()
        {
            //Arrange-create the mock repository
            Mock<IProductRepository> _mock = new Mock<IProductRepository>();
            _mock.Setup(m=>m.Products).Returns((new Product[] {
                new Product{ProductID=1, Name="P1", Category="Cat1"},
                new Product{ProductID=1, Name="P2", Category="Cat2" },
                new Product{ProductID=1, Name="P3", Category="Cat1" },
                new Product{ProductID=1, Name="P4", Category="Cat2" },
                new Product{ProductID=1, Name="P5", Category="Cat3" }
            }).AsQueryable<Product>());

            //Arrange-create a controller and make the page size 3 items
            ProductController controller = new ProductController(_mock.Object);
            controller.PageSize = 3;

            //Act
            Product[] result = (controller.List("Cat2", 1).ViewData.Model as ProductListViewModel)
                .Products.ToArray();

            //Assert
            Assert.AreEqual(2, result.Length);
            Assert.True(result[0].Name == "P2" && result[0].Category == "Cat2");
            Assert.True(result[1].Name == "P4" && result[0].Category == "Cat2");
        }
    }
}
