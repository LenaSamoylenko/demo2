﻿using NUnit.Framework;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportStore.Tests
{
    [TestFixture]
    class CartTests
    {
        [TestCase]
        public void Can_Add_New_Line()
        {
            //Arrange - create some test products
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };

            //Arrange - create a new cart
            Cart _target = new Cart();

            //Act
            _target.AddItem(p1, 1);
            _target.AddItem(p2, 1);
            CartLine[] results = _target.Lines.ToArray();

            //Assert 
            Assert.AreEqual(2, results.Length);
            Assert.AreEqual(p1, results[0].Product);
            Assert.AreEqual(p2, results[1].Product);
        }

        [TestCase]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            //Arrange - create some test products
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };

            //Arrange - create new card
            Cart _target = new Cart();

            //Act
            _target.AddItem(p1, 1);
            _target.AddItem(p2, 1);
            _target.AddItem(p1, 10);
            CartLine[] _results = _target.Lines
                .OrderBy(c => c.Product.ProductID).ToArray();

            //Assert
            Assert.AreEqual(2, _results.Length);
            Assert.AreEqual(11, _results[0].Quantity);
            Assert.AreEqual(1, _results[1].Quantity);
        }

        [TestCase]
        public void Can_Remove_Line()
        {
            //Arrange - create some test products
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };
            Product p3 = new Product { ProductID = 3, Name = "P3" };

            //Arrange - create a new card
            Cart _target = new Cart();

            //Arrange - add some products to the card
            _target.AddItem(p1, 1);
            _target.AddItem(p2, 3);
            _target.AddItem(p3, 5);
            _target.AddItem(p2, 1);

            //Act
            _target.RemoveLine(p2);

            //Assert
            Assert.AreEqual(0, _target.Lines.Where(c => c.Product == p2).Count());
            Assert.AreEqual(2, _target.Lines.Count());
        }

        [TestCase]
        public void Calculate_Card_Total()
        {
            //Arrange - create some test products
            Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100M };
            Product p2 = new Product { ProductID = 2, Name = "P2", Price = 50M };

            //Arrange - create a new card
            Cart _target = new Cart();

            //Act
            _target.AddItem(p1, 1);
            _target.AddItem(p2, 1);
            _target.AddItem(p1, 3);
            decimal _result = _target.ComputeTotalValue();

            //Assert
            Assert.AreEqual(450M, _result);
        }

        [TestCase]
        public void Can_Clear_Contents()
        {
            //Arrange - create some test products
            Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100M };
            Product p2 = new Product { ProductID = 2, Name = "P2", Price = 50M };

            //Arrange - create a new cart
            Cart _target = new Cart();

            //Arrange - add some items
            _target.AddItem(p1, 1);
            _target.AddItem(p2, 1);

            //Act
            _target.Clear();

            //Assert
            Assert.AreEqual(0, _target.Lines.Count());
        }
    }
}
