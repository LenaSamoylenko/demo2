﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Moq;
using NUnit.Framework;
using SportsStore.Infrastructure;
using SportsStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SportStore.Tests
{
    [TestFixture]
    public class PageLinkTagHelperTests
    {
        [TestCase]
        public void Can_Generate_Page_Links()
        {
            //Arrange
            Mock<IUrlHelper> _urlHelper = new Mock<IUrlHelper>();
            _urlHelper.SetupSequence(x => x.Action(It.IsAny<UrlActionContext>()))
                .Returns("Test/Page1")
                .Returns("Test/Page2")
                .Returns("Test/Page3");

            Mock<IUrlHelperFactory> _urlHelperFactory = new Mock<IUrlHelperFactory>();
            _urlHelperFactory.Setup(f => f.GetUrlHelper(It.IsAny<ActionContext>()))
                .Returns(_urlHelper.Object);

            PageLinkTagHelper _helper = new PageLinkTagHelper(_urlHelperFactory.Object)
            {
                PageModel = new PagingInfo
                {
                    CurrentPage = 2,
                    TotalItems = 28,
                    ItemPerPage = 10
                },
                PageAction = "Test"
            };

            TagHelperContext _ctx = new TagHelperContext(
                new TagHelperAttributeList(),
                new Dictionary<object, object>(), "");

            Mock<TagHelperContent> _content = new Mock<TagHelperContent>();
            TagHelperOutput _output = new TagHelperOutput("div",
                new TagHelperAttributeList(),
                (cache, encoer) => Task.FromResult(_content.Object));

            //Act
            _helper.Process(_ctx, _output);

            //Assert
            string _result = String.Concat(@"<a href=""Test/Page1"">1</a>",
                @"<a href=""Test/Page2"">2</a>",
                @"<a href=""Test/Page3"">3</a>");
            Assert.AreEqual(_result, _output.Content.GetContent());
        }
    }
}
