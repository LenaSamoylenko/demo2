﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.AspNetCore.Routing;
using Moq;
using NUnit.Framework;
using SportsStore.Componenets;
using SportsStore.Controllers;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportStore.Tests
{
    [TestFixture]
    class NavigationMenuViewComponentTests
    {
        [TestCase]
        public void Can_Select_Categories()
        {
            //Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[] {
                new Product{ProductID=1, Name="P1", Category="Apples"},
                new Product{ProductID=2, Name="P2", Category="Apples" },
                new Product{ProductID=3, Name="P3", Category="Plums" },
                new Product{ProductID=4, Name="P4", Category="Oranges" }
            }).AsQueryable<Product>());

            NavigationMenuViewComponent _target = new NavigationMenuViewComponent(mock.Object);

            //Act=get the set of categories
            string[] results = ((IEnumerable<string>)(_target.Invoke() as ViewViewComponentResult).ViewData.Model).ToArray();

            //Asssert
            Assert.True(Enumerable.SequenceEqual(new string[] { "Apples", "Oranges", "Plums" }, results));
        }

        [TestCase]
        public void Indicates_Selected_Category()
        {
            //Arrange
            string _categoryToSelect = "Apples";
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[] {
                new Product{ProductID=1, Name="P1", Category="Apples"},
                new Product{ProductID=4, Name="P2", Category="Oranges" }
            }).AsQueryable<Product>());

            NavigationMenuViewComponent target = new NavigationMenuViewComponent(mock.Object);
            target.ViewComponentContext = new ViewComponentContext
            {
                ViewContext = new ViewContext { RouteData = new RouteData() }
            };
            target.RouteData.Values["category"] = _categoryToSelect;

            //Act
            string _result = (string)(target.Invoke() as ViewViewComponentResult).ViewData["SelectedCategory"];

            //Assert
            Assert.AreEqual(_categoryToSelect, _result);
        }

        [TestCase]
        public void Generate_Category_Specific_ProductCount()
        {
            //Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[] {
                new Product{ProductID=1, Name="P1", Category="Cat1"},
                new Product{ProductID=2, Name="P2", Category="Cat2" },
                new Product{ProductID=3, Name="P3", Category="Cat1" },
                new Product{ProductID=4, Name="P4", Category="Cat2" },
                new Product{ProductID=5, Name="P5", Category="Cat3" }
            }).AsQueryable<Product>());

            ProductController target = new ProductController(mock.Object);
            target.PageSize = 3;

            Func<ViewResult, ProductListViewModel> GetModel = result =>
                  result?.ViewData?.Model as ProductListViewModel;

            //Act
            int? res1 = GetModel(target.List("Cat1")) ? .PagingInfo.TotalItems;
            int? res2 = GetModel(target.List("Cat2"))?.PagingInfo.TotalItems;
            int? res3 = GetModel(target.List("Cat3"))?.PagingInfo.TotalItems;
            int? resAll = GetModel(target.List(null))?.PagingInfo.TotalItems;

            //Assert
            Assert.AreEqual(2, res1);
            Assert.AreEqual(2, res2);
            Assert.AreEqual(1, res3);
            Assert.AreEqual(5, resAll);

        }

    }
}
