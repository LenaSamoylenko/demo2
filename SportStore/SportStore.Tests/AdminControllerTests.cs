﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using SportsStore.Controllers;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportStore.Tests
{
    [TestFixture]
    class AdminControllerTests
    {
        [TestCase]
        public void Index_Contains_All_Products()
        {
            //Arrange - create the mock repository
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product{ProductID=1, Name="P1" },
                new Product{ProductID=2, Name="P2" },
                new Product{ProductID=3, Name="P3" }
            }.AsQueryable<Product>());

            //Arrange - create a controller
            AdminController target = new AdminController(mock.Object);

            //Action
            Product[] result
                = GetViewModel<IEnumerable<Product>>(target.Index())?.ToArray();

            //Assert
            Assert.AreEqual(3, result.Length);
            Assert.AreEqual("P1", result[0].Name);
            Assert.AreEqual("P2", result[1].Name);
            Assert.AreEqual("P3", result[2].Name);
        }

        private T GetViewModel<T>(IActionResult result) where T : class
        {
            return (result as ViewResult)?.ViewData.Model as T;
        }

        [TestCase]
        public void Can_Edit_Product()
        {
            //Arrange - create the mock repository
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product{ProductID=1, Name="P1" },
                new Product{ProductID=2, Name="P2" },
                new Product{ProductID=3, Name="P3" }
            }.AsQueryable<Product>());

            //Arrange - create the controller
            AdminController target = new AdminController(mock.Object);

            //Act
            Product p1 = GetViewModel<Product>(target.Edit(1));
            Product p2 = GetViewModel<Product>(target.Edit(2));
            Product p3 = GetViewModel<Product>(target.Edit(3));

            //Assert
            Assert.AreEqual(1, p1.ProductID);
            Assert.AreEqual(2, p2.ProductID);
            Assert.AreEqual(3, p3.ProductID);
        }

        [TestCase]
        public void Cannot_Edit_Noneexsistent_Product()
        {
            //Arrange - create the mock repository
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product{ProductID=1, Name="P1" },
                new Product{ProductID=2, Name="P2" },
                new Product{ProductID=3, Name="P3" }
            }.AsQueryable<Product>());

            //Arrange - create the controller
            AdminController target = new AdminController(mock.Object);

            //Act
            Product result = GetViewModel<Product>(target.Edit(4));

            //Assert
            Assert.IsNull(result);
        }

        [TestCase]
        public void Can_Save_Valid_Changes()
        {
            //Arrange - create mock-repository
            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            //Arrange - create mock temp-data
            Mock<ITempDataDictionary> tempData = new Mock<ITempDataDictionary>();

            //Arrange - create the controller
            AdminController target = new AdminController(mock.Object)
            { TempData = tempData.Object };

            //Arrange - create a product
            Product product = new Product { Name = "Test" };

            //Act - try to save the product
            IActionResult result = target.Edit(product);

            //Assert - check that the repository was called
            mock.Verify(m => m.SaveProduct(product));

            //Assert - check the result type is a redirection
            Assert.IsInstanceOf<RedirectToActionResult>(result);
            Assert.AreEqual("Index", (result as RedirectToActionResult).ActionName);
        }

        [TestCase]
        public void Cannon_Save_Invalid_Changes()
        {
            // Arrange - create mock repository
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            // Arrange - create the controller
            AdminController target = new AdminController(mock.Object);
            // Arrange - create a product
            Product product = new Product { Name = "Test" };
            // Arrange - add an error to the model state
            target.ModelState.AddModelError("error", "error");

            // Act - try to save the product
            IActionResult result = target.Edit(product);

            // Assert - check that the repository was not called
            mock.Verify(m => m.SaveProduct(It.IsAny<Product>()), Times.Never());
            // Assert - check the method result type
            Assert.IsInstanceOf<ViewResult>(result);
        }

        [TestCase]
        public void Can_Delete_Valid_Products()
        {
            // Arrange - create a Product
            Product prod = new Product { ProductID = 2, Name = "Test" };

            // Arrange - create the mock repository
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product {ProductID = 1, Name = "P1"},
                prod,
                new Product {ProductID = 3, Name = "P3"},
            }.AsQueryable<Product>());

            // Arrange - create the controller
            AdminController target = new AdminController(mock.Object);

            // Act - delete the product
            target.Delete(prod.ProductID);

            // Assert - ensure that the repository delete method was
            // called with the correct Product
            mock.Verify(m => m.DeleteProduct(prod.ProductID));
        }

    }
}

