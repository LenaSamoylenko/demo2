﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        #region Fields
        private IProductRepository _repository;
        public int PageSize = 4;
        #endregion

        #region Constructors
        public ProductController(IProductRepository repository)
        {
            _repository = repository;
        }
        #endregion

        #region Methods
        public ViewResult List(string category, int productPage = 1)
            => View(
                new ProductListViewModel
                {
                    Products = _repository.Products
                        .Where(p=>category==null||p.Category==category)
                        .OrderBy(p => p.ProductID)
                        .Skip((productPage - 1) * PageSize)
                        .Take(PageSize),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = productPage,
                        ItemPerPage = PageSize,
                        TotalItems =category==null?
                            _repository.Products.Count():
                            _repository.Products.Where(e=>
                            e.Category==category).Count()
                    },
                    CurrentCategory=category
                });

        #endregion
    }
}