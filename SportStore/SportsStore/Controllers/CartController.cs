﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Infrastructure;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class CartController : Controller
    {
        #region Fields
        private IProductRepository _repository;
        private Cart _cart;
        #endregion

        #region COnstructors
        public CartController(IProductRepository repository, Cart cartService)
        {
            _repository = repository;
            _cart = cartService;
        }
        #endregion

        #region Methods
        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel {
                Cart=GetCart(),
                ReturnUrl=returnUrl
            });
        }

        public RedirectToActionResult AddToCard(int productID, string returnUrl)
        {
            Product _product = _repository.Products
                .FirstOrDefault(p => p.ProductID == productID);

            if (_product != null)
            {
                _cart.AddItem(_product, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToActionResult RemoveFromCart(int productID, string returnUrl)
        {
            Product _product = _repository.Products
                .FirstOrDefault(p => p.ProductID == productID);

            if (_product != null)
            {
                _cart.RemoveLine(_product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        private Cart GetCart()
        {
            Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();
            return cart;
        }

        private void SaveCart(Cart cart)
        {
            HttpContext.Session.SetJson("Cart", cart);
        }
        
        #endregion
    }
}

