﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Componenets
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        #region Fields
        private IProductRepository _repository;
        #endregion

        #region Constructors
        public NavigationMenuViewComponent(IProductRepository repository) {
            _repository = repository;
        }
        #endregion

        #region Methods
        public IViewComponentResult Invoke() {
            ViewBag.SelectedCategory = RouteData?.Values["category"];
            return View(_repository.Products
                .Select(x => x.Category)
                .Distinct()
                .OrderBy(x => x));
        }
        #endregion

    }
}
