﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Infrastructure
{
    public static class UrlExtensions
    {
        public static string PathAndQuary(this HttpRequest _request) =>
            _request.QueryString.HasValue
            ? $"{_request.Path}{_request.QueryString}"
            : _request.Path.ToString();
    }
}
