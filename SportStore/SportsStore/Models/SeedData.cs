﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class SeedData
    {
        private static Dictionary<int, string> _category = new Dictionary<int, string> {
            [1] = "Watersport",
            [2] = "Soccer",
            [3] = "Chess"
        };

        public static void EnsurePopulated(IApplicationBuilder app) {
            ApplicationDbContext _context = app.ApplicationServices
                .GetRequiredService<ApplicationDbContext>();
            _context.Database.Migrate();
            if (!_context.Products.Any()) {
                _context.Products.AddRange(
                    new Product {
                        Name = "Kayak",
                        Description = "A boat for one person",
                        Category = _category[1],
                        Price = 275
                    },
                    new Product {
                        Name = "Lifejacket",
                        Description = "Protective and fashinable",
                        Category = _category[1],
                        Price = 48.95m
                    },
                    new Product {
                        Name = "Soccer Ball",
                        Description = "FIFA-approved size and weight",
                        Category = _category[2],
                        Price = 19.50m
                    },
                    new Product {
                        Name = "Corner Flags",
                        Description = "Give your plaing fields a professional touch",
                        Category = _category[2],
                        Price = 34.95m
                    },
                    new Product {
                        Name = "Stadium",
                        Description = "Flat-packed 35,000-seat stadium",
                        Category = _category[2],
                        Price = 79500
                    },
                    new Product {
                        Name = "Thinking Cap",
                        Description = "Improve brain efficiency by 75%",
                        Category = _category[3],
                        Price = 16
                    },
                    new Product {
                        Name = "Unsteady Chair",
                        Description = "Secretly give your opponent a disadvantege",
                        Category = _category[3],
                        Price = 29.95m
                    },
                    new Product {
                        Name = "Human Chess Board",
                        Description = "A fun game for family",
                        Category = _category[3],
                        Price = 75
                    },
                    new Product {
                        Name = "Bling-Bling King",
                        Description = "Gold-plated, diamond-studded King",
                        Category = _category[3],
                        Price = 1200
                    }
                );

                _context.SaveChanges();
            }
        }
    }
}
